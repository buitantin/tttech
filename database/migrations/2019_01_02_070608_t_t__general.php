<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TTGeneral extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tt_general', function (Blueprint $table) {
            $table->increments('id');
             $table->string('name');
              $table->string('phone');
               $table->string('email');
                $table->string('address');

                
                 $table->text('description')->nullable();
                  $table->string('seo_title',65)->nullable();
                   $table->text('seo_description')->nullable();
                   $table->text("social")->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tt_general');
    }
}
