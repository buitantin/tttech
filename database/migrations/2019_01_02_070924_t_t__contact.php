<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TTContact extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tt_contact', function (Blueprint $table) {
            $table->increments('id');
             $table->string("name");
              $table->string("email")->nullable();
               $table->string("phone")->nullable();
                $table->string("subject")->nullable();
                $table->text("content")->nullable();

                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tt_contact');
    }
}
