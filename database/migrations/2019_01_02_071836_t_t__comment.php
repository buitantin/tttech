<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TTComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tm_comment', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name")->nullable();
            $table->string("email")->nullable();
            $table->text("comment")->nullable();

            $table->enum("status",['1','2'])->default('1')->nullable();
            $table->integer("cid_parent")->default(0)->nullable();
            $table->integer("cid_project")->nullable();
            $table->integer("cid_template")->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tm_comment');
    }
}
