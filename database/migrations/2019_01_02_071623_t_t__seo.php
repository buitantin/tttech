<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TTSeo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tt_seo', function (Blueprint $table) {
            $table->increments('id');
            $table->string("links")->nullable();
            $table->string('seo_title',65)->nullable();
            $table->text('seo_description')->nullable();

            $table->string('footer_title',65)->nullable();
            $table->string("footer_picture")->nullable();
            $table->text('footer_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tt_seo');
    }
}
