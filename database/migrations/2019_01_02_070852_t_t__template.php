<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TTTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tt_template', function (Blueprint $table) {
            $table->increments('id');
          
             $table->string('name');
             $table->string("alias");
           
             $table->string("price")->nullable();
          


             $table->string("picture")->nullable();
             $table->text("multi_picture")->nullable();

             $table->text('description')->nullable();
        


             $table->integer('counter')->default(0)->nullable();
             $table->integer("cid_user");
             $table->integer("cid_cate");

             $table->enum("status",['1','2'])->default('1')->nullable();
             $table->enum("is_home",['1','2'])->default('1')->nullable();   
            $table->enum("is_buy",['1','2'])->default('1')->nullable();   
                    
                  $table->string('seo_title',65)->nullable();
                   $table->text('seo_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tt_template');
    }
}
