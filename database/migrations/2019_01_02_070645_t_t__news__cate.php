<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TTNewsCate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tt_news_cate', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name");
            $table->string("alias");
            $table->integer("orderby")->default(1)->nullable();
            $table->enum("status",['1','2'])->default('1')->nullable();

            $table->string('seo_title',65)->nullable();
            $table->text('seo_description')->nullable();
              

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tt_news_cate');
    }
}
